﻿#include "othello.h"

LRESULT WINAPI Othello::Cell::WndProc(Window* win, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg)
	{
		case WM_LBUTTONUP:
		{
			RECT rc;
			int x = LOWORD(lp), y = HIWORD(lp);
			GetWindowRect(win->hWnd(), &rc);
			if (x >= 0 && x <= rc.right-rc.left && y >= 0 && y <= rc.bottom-rc.top)
			{
				SendMessage(Parent(), WM_CELLPUSHED, reinterpret_cast<WPARAM>(this), 0);
			}
			return CallOrigWindowProc(msg, wp, lp);
			break;
		}
		default:
			return CallOrigWindowProc(msg, wp, lp);
	}
	return 0;
}

enum Othello::Cell::Color Othello::Cell::Color()
{
	auto text = Text();
	enum Color color;
	if (wcscmp(text->c_str(), TEXT("")) == 0)        color = ColorNone;
	else if (wcscmp(text->c_str(), TEXT("●")) == 0) color = ColorBlack;
	else if (wcscmp(text->c_str(), TEXT("○")) == 0) color = ColorWhite;
	else                                             color = ColorInvalid;
	return color;
}
void Othello::Cell::Color(enum Color color)
{
	// Invalidが指定されたら例外を投げるようにしたい
	//if (color == Invalid) throw;
	if (color == ColorNone)       Text(TEXT(""));
	else if (color == ColorBlack) Text(TEXT("●"));
	else if (color == ColorWhite) Text(TEXT("○"));
}
