﻿#ifdef _DEBUG
#define _CRT_SECURE_NO_WARNINGS
#include <windows.h>
#include <cstdio>
#include <cstdlib>
#include <cstdarg>

void TRACE(wchar_t* format, ...)
{
	va_list ap;
	int buflen = 0;
	int len;
	wchar_t* text = NULL;

	va_start(ap, format);
	do
	{
		buflen += 256;
		if (text) delete [] text;
		text = new wchar_t[buflen];
		len = _vsnwprintf(text, buflen, format, ap);
	}
	while (!(len >= 0 && len < buflen));
	va_end(ap);

	OutputDebugString(text);

	delete [] text;
}
#else
void TRACE(wchar_t* format, ...)
{
}
#endif
