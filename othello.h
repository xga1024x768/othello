﻿#pragma once

#include "win32wrapper.h"
#include "debug.h"

using namespace Win32Wrapper;

class Othello
{
public:
	class Board;
	class Cell : public Button
	{
		LRESULT WINAPI WndProc(Window* win, UINT msg, WPARAM wp, LPARAM lp);
		Board* board_;
	public:
		// CellクラスからBoardクラスに送られるウィンドウメッセージ
		enum
		{
			// セルがクリックされた
			WM_CELLPUSHED = WM_APP+1,
		};
		// セルの状態
		enum Color
		{
			ColorInvalid=-1,
			ColorNone,
			ColorBlack,
			ColorWhite,
		};

		// セルの状態の取得・変更・判定
		enum Color Color();
		void Color(enum Color color);
		bool isColor(enum Color color) { return Color() == color; }
		bool isNone()  { return isColor(ColorNone); }
		bool isBlack() { return isColor(ColorBlack); }
		bool isWhite() { return isColor(ColorWhite); }

		
		Cell(Board* board, HWND hParent, int x, int y, int width, int height)
			: Button(hParent, TEXT(""), x, y, width, height)
		{
			EnableSubclassing();
			board_ = board;
		}
	};

	class BoardClass : public WindowClass
	{
		LRESULT WINAPI WndProc(Window* win, UINT msg, WPARAM wp, LPARAM lp)
		{
			return DefWindowProc(win->hWnd(), msg, wp, lp);
		}
	public:
		BoardClass() : WindowClass(TEXT("OthelloBoard"), WS_CHILD | WS_VISIBLE) {}
	};

	class Board : public Window
	{
	public:
		static const int BoardWidth_  = 8;	// 横のマスの数
		static const int BoardHeight_ = 8;	// 縦のマスの数

		enum
		{
			// 指定された場所に石が既に置かれている
			ALREADY_SET = -1,
			// 置けない場所に石を置こうとした
			INVALID_POSITION = 0,
		};

		// 親ウィンドウに送られるウィンドウメッセージ
		enum
		{
			// セルがクリックされた
			// Cell::WM_CELLPUSHEDとは違い押されたセルの場所がwparamに入る
			WM_CELLPUSHED = WM_APP + 2,
		};

	private:
		// クラス内でstatic BoardClass brdcls_;しても外部参照が未解決
		// といわれてできなかったのでそれと同等のことを行う関数
		static BoardClass& GetBoardClass()
		{
			static BoardClass brdcls;
			return brdcls;
		}

		// 場に存在するすべてのマス
		std::shared_ptr<Cell> cells_[BoardWidth_][BoardHeight_];

		LRESULT WINAPI WndProc(Window* win, UINT msg, WPARAM wp, LPARAM lp);
		void OnCellPushed(Cell*);
		void OnSize(int width, int height);

		enum FillCell
		{
			TopLeft  = 1,	// 左上
			Top      = 2,	// 上
			TopRight = 4,	// 右上
			Left     = 8,	// 左
			Right    = 16,	// 右
			BottomLeft  = 32,	// 左下
			Bottom      = 64,	// 下
			BottomRight = 128,	// 右下
		};

		/*
		 * Cellへのポインタからマスの場所を調べる
		 *
		 * @params
		 *   cell: Cellへのポインタ
		 *
		 * @return
		 *   x: X位置
		 *   y: Y位置
		 *   Cellが存在しなかった場合、x,y共に-1が入る
		 */
		void GetCellPosFromPointer(Cell* cell, int& x, int& y);

		/*
		 * 指定した場所が盤外かどうか
		 *
		 * @params
		 *   x: X位置
		 *   y: Y位置
		 *
		 * @return
		 *   0のとき盤外が指定されている
		 *   それ以外のとき盤内が指定されている
		 */
		int isOverflow(int x, int y)
		{
			return x < 0 || x >= BoardWidth_ || y < 0 || y >= BoardHeight_;
		}
	public:
		Board(HWND hWnd, int x, int y, int width, int height);

		/*
		 * 文字列を渡して場を初期化
		 *
		 * * xが黒、oが白になり、それ以外の場合何も置かれない
		 * * 行の長さ>BoardWidth_ならBoardWidth_を超えた部分は無視される
		 * * 行の長さ<BoardWidth_なら足りない部分は何も置かれない
		 * * 列の長さ>BoardHeight_ならBoardHeight_を超えた部分は無視される
		 * * 列の長さ<BoardHeight_なら足りない部分は何も置かれない
		 *
		 * 例:
		 * board->InitByString("x xxxxxx\nx xxxxxx\nxoxxxxxx\nxooxxxxx\nxoooxxxx\nxoxxoxxx\nxoxxxoxx\nxooxxxxx");
		 */
		void InitByString(const char* str)
		{
			int i, j;
			int x, y;
			for (i = 0; i < BoardWidth_; i++)
			{
				for (j = 0; j < BoardHeight_; j++)
				{
					cells_[i][j]->Color(Othello::Cell::ColorNone);
				}
			}
			for (i = y = 0; str[i] && y < BoardHeight_; i++, y++)
			{
				for (x = 0; str[i] && str[i] != '\n' && x < BoardWidth_; i++, x++)
				{
					if (str[i] == 'x')
					{
						cells_[x][y]->Color(Othello::Cell::ColorBlack);
					}
					else if (str[i] == 'o')
					{
						cells_[x][y]->Color(Othello::Cell::ColorWhite);
					}
					else
					{
						cells_[x][y]->Color(Othello::Cell::ColorNone);
					}
				}
			}
		}

		/*
		 * 場の初期化
		 */
		void Init()
		{
			InitByString("\n\n\n   xo\n   ox");
		}

		/*
		 * 石をおくことができるか確認
		 *
		 * @return
		 *   -1: 指定された場所にすでに石が置かれている
		 *    0: 周りに石がひとつもない
		 */
		unsigned int CanPutTo(int x, int y, enum Cell::Color color);

		/*
		 * 石を置く
		 * 間に挟まれた部分は色が変わる
		 *
		 * @return
		 *   ALREADY_SET:      既に石が置かれている
		 *   INVALID_POSITION: 置くことができない場所(周りに石がないなど)に置こうとした
		 *   それ以外:         色が変わった数 (プレイヤーが石を置いた場所も含む)
		 */
		int Put(int x, int y, enum Cell::Color color);

		/*
		 * 置く場所が存在するかどうか
		 * XXX: わざわざ全探索しているから効率が悪い
		 *
		 * @return
		 *   0: 置く場所がない
		 *   1: 黒を置く場所がある
		 *   2: 白を置く場所がある
		*/
		unsigned int hasCellToPut();

		/*
		 * それぞれのマスの数
		 * XXX: 呼び出すたびいちいちすべてのマスを確認するので非効率
		 *
		 * @return
		 *   マスの数
		 */
		unsigned int CountDisc(enum Cell::Color color)
		{
			int i, j;
			unsigned int count = 0;
			for (i = 0; i < BoardWidth_; i++)
			{
				for (j = 0; j < BoardHeight_; j++)
				{
					if (cells_[i][j]->isColor(color)) count++;
				}
			}
			return count;
		}
		unsigned int Black() { return CountDisc(Cell::ColorBlack); }
		unsigned int White() { return CountDisc(Cell::ColorWhite); }
		unsigned int None()  { return CountDisc(Cell::ColorNone);  }
	};

	struct CellPos
	{
		int x;
		int y;
	};
	class Player
	{
	protected:
		Othello* othello_;
		std::wstring name_;
		enum Cell::Color color_;	// どちらの色か
	public:
		Player() {}
		Player(std::wstring name, enum Cell::Color color)
			: othello_(NULL), name_(name), color_(color) {}

		virtual void Turn() = 0;
		virtual void CellClicked(int x, int y){}

		Othello* SetOthello(Othello* othello) { return othello_ = othello; }
		Othello* GetOthello()                 { return othello_; }
		enum Cell::Color SetColor(enum Cell::Color color) { return color_ = color; }
		enum Cell::Color GetColor()                       { return color_; }
	};

private:
	std::shared_ptr<Window> parent_;
	std::shared_ptr<Board>  board_;
	volatile enum Cell::Color turn_;
	std::shared_ptr<Player> black_player_;
	std::shared_ptr<Player> white_player_;
	HANDLE hTurnThread;

public:
#define TC_NOPLACE(wp)  (((wp)&3)==0)                           // 双方共におくことのできる場所がない
#define TC_BLACK(wp)    ((wp)&1)                                // 黒がおくことのできる場所がある
#define TC_WHITE(wp)    ((wp)&2)                                // 白がおくことのできる場所がある
#define TC_FINISHED(wp) (((wp)&12)!=0)                          // 決着がついているかどうか
#define TC_DRAW(wp)     (TC_FINISHED(wp) && ((wp)&12) == 12)    // 引き分け
#define TC_BLACKWIN(wp) (!TC_DRAW(wp) && ((wp)&4))              // 黒の勝利
#define TC_WHITEWIN(wp) (!TC_DRAW(wp) && ((wp)&8))              // 白の勝利
	enum
	{
		// ターンが変わったとき親ウィンドウに送られる
		// WPARAMの値の論理和によって以下の判断ができる
		// TC_*マクロを使うことでも判断可能
		// num bit
		//   0[0-1]:双方共における場所がない
		//   1[ 0 ]:黒がおける場所がある
		//   2[ 1 ]:白がおける場所がある
		// ( 3[0-1]:双方共における場所がある)
		//   4[ 2 ]:黒が勝利した
		//   8[ 3 ]:白が勝利した
		// (12[2-3]:引き分け)
		//  16[ 4 ]:黒のターン
		//  32[ 5 ]:白のターン
		// またLPARAMの上位に黒の枚数、下位に白の枚数が入る
		WM_TURNCHANGED = WM_APP + 10,
	};

	Othello(HWND hParent, int x, int y, int width, int height) : hTurnThread(0)
	{
		parent_ = std::shared_ptr<Window>(new Window(hParent));
		board_  = std::shared_ptr<Board>(new Board(parent_->hWnd(), x, y, width, height));
	}
	virtual ~Othello()
	{
		if (hTurnThread) TerminateThread(hTurnThread, 0);
	}
	void SetPlayer(std::shared_ptr<Player> black_player, std::shared_ptr<Player> white_player)
	{
		black_player_.swap(black_player);
		white_player_.swap(white_player);
	}
	void Init()
	{
		board_->Init();
		SetTurn(Cell::ColorBlack);
		// TODO: TerminateThreadでスレッドを殺すのはあまりよくない気がする
		if (hTurnThread) TerminateThread(hTurnThread, 0);
		hTurnThread = CreateThread(NULL, 0, TurnThread, this, 0, NULL);
	}
	static DWORD WINAPI TurnThread(LPVOID lpParameter)
	{
		Othello* this_ = reinterpret_cast<Othello*>(lpParameter);
		int none, black, white;
		WPARAM wp = 0;
		LPARAM lp = 0;
		while (1)
		{
			wp = lp = 0;
			none = this_->GetBoard()->None();
			black = this_->GetBoard()->Black();
			white = this_->GetBoard()->White();
			if (none == 0)
			{
				// すべてのマスが埋まった
				// 黒の勝利
				if (black > white)      wp |= 1 << 2;
				// 白の勝利
				else if (black < white) wp |= 1 << 3;
				// 引き分け
				else                    wp |= 1 << 2 | 1 << 3;
			}
			else
			{
				// まだすべてのマスが埋まっていない
				
				// すべて黒になったので黒の勝利
				if (white == 0) wp |= 1 << 2;
				// すべて白になったので白の勝利
				if (black == 0) wp |= 1 << 3;

				int ret;
				ret = this_->GetBoard()->hasCellToPut();

				// 黒はまだおくことのできる場所がある
				wp |= ret&1 ? 1<<0 : 0;
				// 白はまだおくことのできる場所がある
				wp |= ret&2 ? 1<<1 : 0;

				if (TC_NOPLACE(wp))
				{
					// もう両方置くことのできる場所が存在しないので勝敗をつける

					// 黒のほうが多いので黒の勝利
					if      (black > white) wp |= 1 << 2;
					// すべて白になったので白の勝利
					else if (black < white) wp |= 1 << 3;
					// 引き分け
					else                    wp |= 1 << 2 | 1 << 3;
				}
			}

			// どちらのターンにするかを決める
			// XXX: どちらも置けなくなったりしたときのことを考えていない
			if (this_->GetTurn() == Cell::ColorBlack && !(wp&(1<<0)))
			{
				this_->SetTurn(Cell::ColorWhite);
			}
			if (this_->GetTurn() == Cell::ColorWhite && !(wp&(1<<1)))
			{
				this_->SetTurn(Cell::ColorBlack);
			}

			// どちらのターンになったのかをwparamにいれる
			if (!TC_FINISHED(wp))
			{
				if (this_->GetTurn() == Cell::ColorBlack) wp |= 1<<4;
				else                                      wp |= 1<<5;
			}

			// 親ウィンドウに対してターンが変わったことを通知
			lp = MAKELPARAM(this_->GetBoard()->White(), this_->GetBoard()->Black());
			SendMessage(this_->GetParent()->hWnd(), WM_TURNCHANGED, wp, lp);

			// もしwpの2bit目か3bit目が立っていたら勝負がついているのでスレッドを終了する
			if (wp&(1<<2) || wp&(1<<3))
			{
				this_->hTurnThread = 0;
				ExitThread(0);
			}

			if (this_->GetTurn() == Cell::ColorBlack)
			{
				this_->black_player_->Turn();
				this_->SetTurn(Cell::ColorWhite);
			}
			else if (this_->GetTurn() == Cell::ColorWhite)
			{
				this_->white_player_->Turn();
				this_->SetTurn(Cell::ColorBlack);
			}
		}
		return 0;
	}
	std::shared_ptr<Board> GetBoard() { return board_; }
	std::shared_ptr<Window> GetParent() { return parent_; }
	enum Cell::Color GetTurn() { return turn_; }
	void SetTurn(enum Cell::Color color) { turn_ = color; }
};
