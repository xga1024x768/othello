﻿#include "win32wrapper.h"
#include <memory>
#include <wchar.h>
#include <sstream>
#include <typeinfo>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "debug.h"
#include "othello.h"
#include "ai.h"

#pragma warning(disable:4996)
#define snwprintf _snwprintf

using namespace Win32Wrapper;

class ManualPlayer : public Othello::Player
{
	volatile int cx_, cy_;
public:
	virtual void Turn()
	{
		cx_ = cy_ = -1;
		while (cx_ == -1 && cy_ == -1);
		othello_->GetBoard()->Put(cx_, cy_, color_);
	}
	void CellClicked(int x, int y)
	{
		cx_ = x;
		cy_ = y;
	}
};

class MainWindow : public WindowClass
{
private:
	std::shared_ptr<Othello> othello_;
	std::shared_ptr<Othello::Player> black_player_, white_player_;
	std::shared_ptr<Othello::Player> change_black_player_, change_white_player_;
	static const int MinWidth_ = 400;
	static const int MinHeight_ = 400;

	LRESULT WINAPI WndProc(Window* win, UINT msg, WPARAM wp, LPARAM lp)
	{
		switch (msg)
		{
			case WM_CREATE:
				win->Text(TEXT("Othello"));
				win->Resize(MinWidth_, MinHeight_);
				othello_ = std::shared_ptr<Othello>(new Othello(win->hWnd(), 0, 30, 300, 300));
				
				black_player_ = std::shared_ptr<ManualPlayer>(new ManualPlayer);
				InitPlayer(black_player_, Othello::Cell::ColorBlack);

				white_player_ = std::shared_ptr<AutoPlayer3>(new AutoPlayer3);
				InitPlayer(white_player_, Othello::Cell::ColorWhite);

				othello_->SetPlayer(black_player_, white_player_);

				InitGame(win, false);
				break;
			case WM_PAINT:
				HDC hdc;
				PAINTSTRUCT ps;
				hdc = BeginPaint(win->hWnd(), &ps);
				OnPaint(hdc, &ps);
				EndPaint(win->hWnd(), &ps);
				break;
			case Othello::Board::WM_CELLPUSHED:
				OnCellPushed(win, LOWORD(wp), HIWORD(wp));
				SetFocus(win->hWnd());
				break;
			case Othello::WM_TURNCHANGED:
				OnTurnChanged(win, wp, HIWORD(lp), LOWORD(lp));
				break;
			case WM_KEYDOWN:
				{
					if (wp == VK_F1)
					{
						static std::wstring helptext;
						if (helptext.empty())
						{
							helptext.append(TEXT("F1\t\tヘルプ\n"));
							helptext.append(TEXT("Ctrl+R\t\t最初からやり直す\n"));
							helptext.append(TEXT("Ctrl+Shift+R\t先攻と後攻を入れ替えてやり直す\n"));
							helptext.append(TEXT("F2\t\t先攻をプレイヤーにする\n"));
							helptext.append(TEXT("F3\t\t先攻をAI1にする\n"));
							helptext.append(TEXT("F4\t\t先攻をAI2にする\n"));
							helptext.append(TEXT("F5\t\t先攻をAI3にする\n"));
							helptext.append(TEXT("Ctrl+F2\t\t後攻をプレイヤーにする\n"));
							helptext.append(TEXT("Ctrl+F2\t\t後攻をAI1にする\n"));
							helptext.append(TEXT("Ctrl+F3\t\t後攻をAI2にする\n"));
							helptext.append(TEXT("Ctrl+F4\t\t後攻をAI3にする\n"));
						}
						MessageBox(win->hWnd(), helptext.c_str(), TEXT("help"), MB_OK);
					}
					else if (wp == VK_F2)
					{
						if (GetKeyState(VK_CONTROL)&0x80)
						{
							auto newplayer = std::shared_ptr<Othello::Player>(new ManualPlayer);
							InitPlayer(newplayer, Othello::Cell::ColorWhite);
							ChangePlayer(nullptr, newplayer);
							MessageBox(win->hWnd(), TEXT("後攻がプレイヤーに変更されました"), TEXT(""), MB_OK);
						}
						else
						{
							auto newplayer = std::shared_ptr<Othello::Player>(new ManualPlayer);
							InitPlayer(newplayer, Othello::Cell::ColorBlack);
							ChangePlayer(newplayer, nullptr);
							MessageBox(win->hWnd(), TEXT("先攻がプレイヤーに変更されました"), TEXT(""), MB_OK);
						}
					}
					else if (wp == VK_F3)
					{
						if (GetKeyState(VK_CONTROL)&0x80)
						{
							auto newplayer = std::shared_ptr<Othello::Player>(new AutoPlayer1);
							InitPlayer(newplayer, Othello::Cell::ColorWhite);
							ChangePlayer(nullptr, newplayer);
							MessageBox(win->hWnd(), TEXT("後攻がAI1に変更されました"), TEXT(""), MB_OK);
						}
						else
						{
							auto newplayer = std::shared_ptr<Othello::Player>(new AutoPlayer1);
							InitPlayer(newplayer, Othello::Cell::ColorBlack);
							ChangePlayer(newplayer, nullptr);
							MessageBox(win->hWnd(), TEXT("先攻がAI1に変更されました"), TEXT(""), MB_OK);
						}
					}
					else if (wp == VK_F4)
					{
						if (GetKeyState(VK_CONTROL)&0x80)
						{
							auto newplayer = std::shared_ptr<Othello::Player>(new AutoPlayer2);
							InitPlayer(newplayer, Othello::Cell::ColorWhite);
							ChangePlayer(nullptr, newplayer);
							MessageBox(win->hWnd(), TEXT("後攻がAI2に変更されました"), TEXT(""), MB_OK);
						}
						else
						{
							auto newplayer = std::shared_ptr<Othello::Player>(new AutoPlayer2);
							InitPlayer(newplayer, Othello::Cell::ColorBlack);
							ChangePlayer(newplayer, nullptr);
							MessageBox(win->hWnd(), TEXT("先攻がAI2に変更されました"), TEXT(""), MB_OK);
						}
					}
					else if (wp == VK_F5)
					{
						if (GetKeyState(VK_CONTROL)&0x80)
						{
							auto newplayer = std::shared_ptr<Othello::Player>(new AutoPlayer3);
							InitPlayer(newplayer, Othello::Cell::ColorWhite);
							ChangePlayer(nullptr, newplayer);
							MessageBox(win->hWnd(), TEXT("後攻がAI3に変更されました"), TEXT(""), MB_OK);
						}
						else
						{
							auto newplayer = std::shared_ptr<Othello::Player>(new AutoPlayer3);
							InitPlayer(newplayer, Othello::Cell::ColorBlack);
							ChangePlayer(newplayer, nullptr);
							MessageBox(win->hWnd(), TEXT("先攻がAI3に変更されました"), TEXT(""), MB_OK);
						}
					}
					else if (GetKeyState(VK_CONTROL)&0x80)
					{
						if (wp == 'R')
						{
							if (GetKeyState(VK_SHIFT)&0x80) InitGame(win, true, true);
							else                            InitGame(win, true, false);
						}
					}
				}
				break;
			case WM_SIZE:
				othello_->GetBoard()->Resize(LOWORD(lp), HIWORD(lp)-30);
				break;
			case WM_DESTROY:
				PostQuitMessage(0);
				break;
			default:
				return DefWindowProc(win->hWnd(), msg, wp, lp);
		}
		return 0;
	}
	void OnPaint(HDC hdc, LPPAINTSTRUCT ps)
	{
		std::wostringstream stream;
		std::wstring str;
		HFONT hOrigFont;
		HFONT hFont = CreateFont(
			17, 0, 0, 0, FW_DONTCARE,
			FALSE, FALSE, FALSE,
			DEFAULT_CHARSET, OUT_CHARACTER_PRECIS,
			CLIP_DEFAULT_PRECIS,
			ANTIALIASED_QUALITY,
			DEFAULT_PITCH,
			TEXT("ＭＳ ゴシック")
		);

		hOrigFont = reinterpret_cast<HFONT>(SelectObject(hdc, hFont));

		str = othello_->GetTurn() == Othello::Cell::ColorBlack ? TEXT("黒") : TEXT("白");
		str += TEXT("の番です");

		str += TEXT("　黒:");
		stream << othello_->GetBoard()->Black();
		str += stream.str();

		stream.str(TEXT(""));
		stream.clear();

		str += TEXT("　白:");
		stream << othello_->GetBoard()->White();
		str += stream.str();

		TextOut(hdc, 5, 5, str.c_str(), str.length());
					
		SelectObject(hdc, hOrigFont);
		DeleteObject(hFont);
	}
	void OnCellPushed(Window* win, int x, int y)
	{
		int ret;
		RedrawStatusLine(win);

		ret = othello_->GetBoard()->CanPutTo(x, y, othello_->GetTurn());
		if (ret == Othello::Board::ALREADY_SET)
		{
			MessageBox(win->hWnd(), TEXT("既に置かれています"), TEXT("error"), MB_OK);
		}
		else if (ret == Othello::Board::INVALID_POSITION)
		{
			MessageBox(win->hWnd(), TEXT("その場所には置けません"), TEXT("error"), MB_OK);
		}
		else
		{
			if (othello_->GetTurn() == Othello::Cell::ColorBlack)
			{
				black_player_->CellClicked(x, y);
			}
			else if (othello_->GetTurn() == Othello::Cell::ColorWhite)
			{
				white_player_->CellClicked(x, y);
			}
		}
	}
	void OnTurnChanged(Window* win, WPARAM wp, WORD black, WORD white)
	{
		wchar_t message[128];

		RedrawStatusLine(win);
		
		message[0] = 0;
		if (TC_NOPLACE(wp))
		{
			if      (black > white) snwprintf(message, 128, TEXT("双方置ける場所がなくなりました。黒の勝利です。"));
			else if (black < white) snwprintf(message, 128, TEXT("双方置ける場所がなくなりました。白の勝利です。"));
			else                    snwprintf(message, 128, TEXT("双方置ける場所がなくなりました。引き分けです。"));
		}
		else if (TC_DRAW(wp)) snwprintf(message, 128, TEXT("引き分けです。"));
		else if (TC_BLACKWIN(wp))
		{
			if (white == 0) snwprintf(message, 128, TEXT("すべての石が黒になりました。黒の勝利です。"));
			else            snwprintf(message, 128, TEXT("黒の勝利です。"));
		}
		else if (TC_WHITEWIN(wp))
		{
			if (black == 0) snwprintf(message, 128, TEXT("すべての石が白になりました。白の勝利です。"));
			else            snwprintf(message, 128, TEXT("白の勝利です。"));
		}
		if (message[0]) MessageBox(win->hWnd(), message, TEXT(""), MB_OK);

		ChangePlayer(nullptr, nullptr);
	}
	void ChangePlayer(std::shared_ptr<Othello::Player> black_player, std::shared_ptr<Othello::Player> white_player)
	{
		// 今すぐプレイヤーを変えることができない場合一度change_*_playerに退避しOnTurnChangedの最後で変える
		// (現在順番が回ってきているプレイヤーはインスタンスを入れ替えると問題が起きるため退避させている)
		if (othello_->GetTurn() == Othello::Cell::ColorBlack)
		{
			if (black_player && black_player != black_player_)
			{
				// 今すぐ変えられないので一時退避させる
				change_black_player_ = black_player;
			}
			if (change_white_player_ && change_white_player_ != white_player_)
			{
				// 一時退避していた白のプレイヤーを変えられる状況になったので変える
				white_player_.swap(change_white_player_);
				change_white_player_.reset();
				othello_->SetPlayer(black_player_, white_player_);
				change_white_player_ = nullptr;
			}
			if (white_player && white_player != white_player_)
			{
				// 今すぐ白のプレイヤーを変えられるので変える
				white_player_.swap(white_player);
				othello_->SetPlayer(black_player_, white_player_);
			}
		}
		else if (othello_->GetTurn() == Othello::Cell::ColorWhite)
		{
			if (white_player && white_player != white_player_)
			{
				// 今すぐ変えられないので一時退避させる
				change_white_player_ = white_player;
			}
			if (change_black_player_ && change_black_player_ != black_player_)
			{
				// 一時退避していた黒のプレイヤーを変えられる状況になったので変える
				black_player_.swap(change_black_player_);
				change_black_player_.reset();
				othello_->SetPlayer(black_player_, white_player_);
				change_black_player_ = nullptr;
			}
			if (black_player && black_player != black_player_)
			{
				// 今すぐ黒のプレイヤーを変えられるので変える
				black_player_.swap(black_player);
				othello_->SetPlayer(black_player_, white_player_);
			}
		}
	}
	void InitGame(Window* win, bool confirm=false, bool reverse=false)
	{
		bool do_init = true;
		if (confirm)
		{
			int yn;
			yn = MessageBox(win->hWnd(), TEXT("ゲームを最初からやり直しますか？"), TEXT(""), MB_YESNO);
			if (yn != IDYES) do_init = false;
		}
		if (do_init)
		{
			RECT rc;
			othello_->Init();
			GetClientRect(win->hWnd(), &rc);
			rc.bottom = 30;
			InvalidateRect(win->hWnd(), &rc, TRUE);

			if (reverse)
			{
				black_player_.swap(white_player_);
				black_player_->SetColor(Othello::Cell::ColorBlack);
				white_player_->SetColor(Othello::Cell::ColorWhite);
				othello_->SetPlayer(black_player_, white_player_);
			}
		}
	}
	void InitPlayer(std::shared_ptr<Othello::Player> player, enum Othello::Cell::Color color)
	{
		player->SetOthello(othello_.get());
		player->SetColor(color);
	}
	void RedrawStatusLine(Window* win)
	{
		RECT rc;
		GetClientRect(win->hWnd(), &rc);
		rc.bottom = 30;
		InvalidateRect(win->hWnd(), &rc, TRUE);
	}
public:
	MainWindow() : WindowClass(TEXT("MainWindow"), WS_OVERLAPPEDWINDOW)
	{
		change_black_player_ = nullptr;
		change_white_player_ = nullptr;
	}
	virtual ~MainWindow()
	{
	}
};

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	// MainWindowの場合はタイトル、それ以外のBoard, Cell等の場合は親ウィンドウをCreate関数で指定できるようにしたい
	std::shared_ptr<MainWindow> wincls(new MainWindow());
	std::shared_ptr<Window> win = wincls->Create();
	MSG msg;
	win->Show(nCmdShow);
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return (int)msg.wParam;
}
