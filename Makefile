CC=x86_64-w64-mingw32-gcc
CFLAGS=-DUNICODE -std=c++11 -Wall
CXX=x86_64-w64-mingw32-g++
CXXFLAGS=${CFLAGS}
LDFLAGS=-static -mwindows
LIBS=-lgdi32
MAKE=make

TARGET=othello.exe
SOURCE=ai.cpp board.cpp cell.cpp debug.cpp main.cpp win32wrapper.cpp

default:
	$(MAKE) $(TARGET)

$(TARGET): ${SOURCE}
	$(CXX) $(CFLAGS) $(LDFLAGS) -o $(TARGET) $(LDFLAGS) $(SOURCE)

