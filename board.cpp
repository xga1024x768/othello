﻿#include "othello.h"

Othello::Board::Board(HWND hParent, int x, int y, int width, int height) : Window(GetBoardClass().Create(hParent)->hWnd())
{
	int i, j;
	Move(x, y);
	Resize(width, height);
	EnableSubclassing();
	for (i = 0; i < BoardWidth_; i++)
	{
		for (j = 0; j < BoardHeight_; j++)
		{
			cells_[i][j] = std::shared_ptr<Cell>(new Cell(
				this,
				hWnd_,
				i*width/BoardWidth_,j*height/BoardHeight_,
				width/BoardWidth_, height/BoardHeight_
				));
		}
	}
}

LRESULT WINAPI Othello::Board::WndProc(Window* win, UINT msg, WPARAM wp, LPARAM lp)
{
	switch (msg)
	{
		case Othello::Cell::WM_CELLPUSHED:
			OnCellPushed(reinterpret_cast<Cell*>(wp));
			break;
		case WM_SIZE:
			OnSize(LOWORD(lp), HIWORD(lp));
			break;
		default:
			return CallOrigWindowProc(msg, wp, lp);
	}
	return 0;
}

void Othello::Board::OnCellPushed(Cell* cell)
{
	int x, y;
	GetCellPosFromPointer(cell, x, y);
	SendMessage(Parent(), WM_CELLPUSHED, MAKEWPARAM(x, y), 0);
}

void Othello::Board::OnSize(int width, int height)
{
	int i, j;
	for (i = 0; i < BoardWidth_; i++)
	{
		for (j = 0; j < BoardHeight_; j++)
		{
			cells_[i][j]->Move(i*width/BoardWidth_, j*height/BoardHeight_);
			cells_[i][j]->Resize(width/BoardWidth_, height/BoardHeight_);
		}
	}
}

void Othello::Board::GetCellPosFromPointer(Cell* cell, int& x, int& y)
{
	int i, j;
	for (i = 0; i < BoardWidth_; i++)
	{
		for (j = 0; j < BoardHeight_; j++)
		{
			if (cell == cells_[i][j].get())
			{
				x = i;
				y = j;
				return;
			}
		}
	}
	if (i >= BoardWidth_ || j >= BoardHeight_)
	{
		x = -1;
		y = -1;
	}
}

unsigned int Othello::Board::CanPutTo(int x, int y, enum Cell::Color color)
{
	int a, b;
	unsigned int flag = 0;

	// 指定された場所に石が存在する場合-1
	if (!cells_[x][y]->isNone()) return -1;

	// 上のマスに同じ色の石が置かれているか確認
	for (b = y-1; !isOverflow(x, b) && !(cells_[x][b]->isColor(color) || cells_[x][b]->isNone()); b--);
	if (b < y-1 && !isOverflow(x, b) && cells_[x][b]->isColor(color)) flag |= Top;

	// 下のマスに同じ色の石が置かれているか確認
	for (b = y+1; !isOverflow(x, b) && !(cells_[x][b]->isColor(color) || cells_[x][b]->isNone()); b++);
	if (b > y+1 && !isOverflow(x, b) && cells_[x][b]->isColor(color)) flag |= Bottom;

	// 左のマスに同じ色の石が置かれているか確認
	for (a = x-1; !isOverflow(a, y) && !(cells_[a][y]->isColor(color) || cells_[a][y]->isNone()); a--);
	if (a < x-1 && !isOverflow(a, y) && cells_[a][y]->isColor(color)) flag |= Left;

	// 右のマスに同じ色の石が置かれているか確認
	for (a = x+1; !isOverflow(a, y) && !(cells_[a][y]->isColor(color) || cells_[a][y]->isNone()); a++);
	if (a  > x+1 && !isOverflow(a, y) && cells_[a][y]->isColor(color)) flag |= Right;

	// 左上のマスに同じ色の石が置かれているか確認
	for (a = x-1, b = y-1; !isOverflow(a, b) && !(cells_[a][b]->isColor(color) || cells_[a][b]->isNone()); a--, b--);
	if (a < x-1 && b < y-1 && !isOverflow(a, b) && cells_[a][b]->isColor(color)) flag |= TopLeft;

	// 右上のマスに同じ色の石が置かれているか確認
	for (a = x+1, b = y-1; !isOverflow(a, b) && !(cells_[a][b]->isColor(color) || cells_[a][b]->isNone()); a++, b--);
	if (a > x+1 && b < y-1 && !isOverflow(a, b) && cells_[a][b]->isColor(color)) flag |= TopRight;

	// 左下のマスに同じ色の石が置かれているか確認
	for (a = x-1, b = y+1; !isOverflow(a, b) && !(cells_[a][b]->isColor(color) || cells_[a][b]->isNone()); a--, b++);
	if (a < x-1 && b > y+1 && !isOverflow(a, b) && cells_[a][b]->isColor(color)) flag |= BottomLeft;

	// 右下のマスに同じ色の石が置かれているか確認
	for (a = x+1, b = y+1; !isOverflow(a, b) && !(cells_[a][b]->isColor(color) || cells_[a][b]->isNone()); a++, b++);
	if (a > x+1 && b > y+1 && !isOverflow(a, b) && cells_[a][b]->isColor(color)) flag |= BottomRight;

	return flag;
}

int Othello::Board::Put(int x, int y, enum Othello::Cell::Color color)
{
	unsigned int flag = 0;	// どこが挟まれているかのフラグ
	int count;	// いくつの石の色が変わったか
	int distance;

	flag = CanPutTo(x, y, color);
	if (flag == -1)
	{
		return ALREADY_SET;

	}
	else if (flag == 0)
	{
		return INVALID_POSITION;
	}
	else
	{
		cells_[x][y]->Color(color);

		count = 0;
		// distanceを増やしながら放射状に実際に石を置いていく
		distance = 1;
		while (flag)
		{
			if (flag & TopLeft)
			{
				if (isOverflow(x-distance, y-distance) || cells_[x-distance][y-distance]->Color() == color)
				{
					flag &= ~TopLeft;
					count--;
				}
				cells_[x-distance][y-distance]->Color(color);
				count++;
			}
			if (flag & Top)
			{
				if (isOverflow(x, y-distance) || cells_[x][y-distance]->Color() == color)
				{
					flag &= ~Top;
					count--;
				}
				cells_[x][y-distance]->Color(color);
				count++;
			}
			if (flag & TopRight)
			{
				if (isOverflow(x+distance, y-distance) || cells_[x+distance][y-distance]->Color() == color)
				{
					flag &= ~TopRight;
					count--;
				}
				cells_[x+distance][y-distance]->Color(color);
				count++;
			}
			if (flag & Left)
			{
				if (isOverflow(x-distance, y) || cells_[x-distance][y]->Color() == color)
				{
					flag &= ~Left;
					count--;
				}
				cells_[x-distance][y]->Color(color);
				count++;
			}
			if (flag & Right)
			{
				if (isOverflow(x+distance, y) || cells_[x+distance][y]->Color() == color)
				{
					flag &= ~Right;
					count--;
				}
				cells_[x+distance][y]->Color(color);
				count++;
			}
			if (flag & BottomLeft)
			{
				if (isOverflow(x-distance, y+distance) || cells_[x-distance][y+distance]->Color() == color)
				{
					flag &= ~BottomLeft;
					count--;
				}
				cells_[x-distance][y+distance]->Color(color);
				count++;
			}
			if (flag & Bottom)
			{
				if (isOverflow(x, y+distance) || cells_[x][y+distance]->Color() == color)
				{
					flag &= ~Bottom;
					count--;
				}
				cells_[x][y+distance]->Color(color);
				count++;
			}
			if (flag & BottomRight)
			{
				if (isOverflow(x+distance, y+distance) || cells_[x+distance][y+distance]->Color() == color)
				{
					flag &= ~BottomRight;
					count--;
				}
				cells_[x+distance][y+distance]->Color(color);
				count++;
			}

			distance++;
			if (x-distance < 0)            flag &= ~TopLeft    | ~Left   | ~BottomLeft;
			if (x+distance > BoardWidth_)  flag &= ~TopRight   | ~Right  | ~BottomRight;
			if (y-distance < 0)            flag &= ~TopLeft    | ~Top    | ~TopRight;
			if (y+distance > BoardHeight_) flag &= ~BottomLeft | ~Bottom | ~BottomRight;
		}
		return count+1;
	}
}

unsigned int Othello::Board::hasCellToPut()
{
	unsigned int flag = 0;
	unsigned int cstret;
	int i, j;
	for (i = 0; i < BoardWidth_; i++)
	{
		for (j = 0; j < BoardHeight_; j++)
		{
			cstret = CanPutTo(i, j, Cell::ColorBlack);
			if (!(flag & 1) && cstret != -1 && cstret != 0)
			{
				flag |= 1;
			}
			cstret = CanPutTo(i, j, Cell::ColorWhite);
			if (!(flag & 2) && cstret != -1 && cstret != 0)
			{
				flag |= 2;
			}
			if (flag & 1 && flag & 2) break;
		}
	}
	return flag;
}
