﻿#pragma once
#include "othello.h"
#include <algorithm>

class AutoPlayer : public Othello::Player
{
protected:
	// 角かどうか
	inline bool IsCorner(int x, int y)
	{
		return (x == 0                            && y == 0) ||
		       (x == 0                            && y == Othello::Board::BoardHeight_-1) ||
		       (x == Othello::Board::BoardWidth_-1 && y == 0) ||
		       (x == Othello::Board::BoardWidth_-1 && y == Othello::Board::BoardHeight_-1);
	}
	// 角付近のマスかどうか
	inline bool IsPreCorner(int x, int y)
	{
		return (x <= 1 && y <= 1) ||
		       (x >= Othello::Board::BoardWidth_-2 && y <= 1) ||
		       (x <= 1 && y >= Othello::Board::BoardHeight_-2) ||
		       (x >= Othello::Board::BoardWidth_-2 && y >= Othello::Board::BoardHeight_-2);
	}
	// 置くところのできる場所をposesに入れて返す
	inline void GetPositionsToPut(std::vector<struct Othello::CellPos>& poses)
	{
		int x, y;
		for (x = 0; x < Othello::Board::BoardWidth_; x++)
		{
			for (y = 0; y < Othello::Board::BoardHeight_; y++)
			{
				if ((int)othello_->GetBoard()->CanPutTo(x, y, color_) > 0)
				{
					struct Othello::CellPos pos = {x, y};
					poses.push_back(pos);
				}
			}
		}
	}
	// ランダムに選んで返す
	inline struct Othello::CellPos& RandomItem(std::vector<struct Othello::CellPos>& vector)
	{
		srand((unsigned int)time(NULL));
		return vector.at(rand()%vector.size());
	}
};

class AutoPlayer1 : public AutoPlayer
{
public:
	void Turn()
	{
		std::vector<struct Othello::CellPos> poses;
		GetPositionsToPut(poses);

		// 見つけた置けそうな場所からランダムに選ぶ
		struct Othello::CellPos& pos = RandomItem(poses);
		othello_->GetBoard()->Put(pos.x, pos.y, color_);
	}
};

class AutoPlayer2 : public AutoPlayer
{
public:
	void Turn()
	{
		std::vector<struct Othello::CellPos> poses;
		GetPositionsToPut(poses);

		// 角でおける場所がないか探しランダムに選ぶ
		{
			std::vector<struct Othello::CellPos> places;
			std::for_each(poses.begin(), poses.end(), [&](struct Othello::CellPos& pos){
				if (IsCorner(pos.x, pos.y))
				{
					places.push_back(pos);
				}
			});
			if (!places.empty())
			{
				struct Othello::CellPos& pos = RandomItem(places);
				othello_->GetBoard()->Put(pos.x, pos.y, color_);
				return;
			}
		}

		// 角付近1マスを除く外側1マスで置ける場所がないか探しランダムに選ぶ
		{
			std::vector<struct Othello::CellPos> places;
			std::for_each(poses.begin(), poses.end(), [&](struct Othello::CellPos& pos){
				if (
				    ((pos.y == 0 || pos.y == Othello::Board::BoardHeight_-1) ||
				     (pos.x == 0 || pos.x == Othello::Board::BoardWidth_-1)) &&
					!IsPreCorner(pos.x, pos.y)
				   )
				{
					places.push_back(pos);
				}
			});
			if (!places.empty())
			{
				struct Othello::CellPos& pos = RandomItem(places);
				othello_->GetBoard()->Put(pos.x, pos.y, color_);
				return;
			}
		}

		// 外側2マス以外でおける場所がないか探しランダムに選ぶ
		{
			std::vector<struct Othello::CellPos> places;
			std::for_each(poses.begin(), poses.end(), [&](struct Othello::CellPos& pos){
				if (pos.x >= 2 && pos.y >= 2 &&
					pos.x <= Othello::Board::BoardWidth_-2 &&
					pos.y <= Othello::Board::BoardHeight_-2)
				{
					places.push_back(pos);
				}
			});
			if (!places.empty())
			{
				struct Othello::CellPos& pos = RandomItem(places);
				othello_->GetBoard()->Put(pos.x, pos.y, color_);
				return;
			}
		}

		// どれも見つからなかった場合ランダムに選ぶ
		struct Othello::CellPos& pos = RandomItem(poses);
		othello_->GetBoard()->Put(pos.x, pos.y, color_);
	}
};

class AutoPlayer3 : public AutoPlayer
{
public:
	void Turn()
	{
		std::vector<struct Othello::CellPos> poses;
		GetPositionsToPut(poses);

		// 角でおける場所がないか探しランダムに選ぶ
		{
			std::vector<struct Othello::CellPos> places;
			std::for_each(poses.begin(), poses.end(), [&](struct Othello::CellPos& pos){
				if (IsCorner(pos.x, pos.y))
				{
					places.push_back(pos);
				}
			});
			if (!places.empty())
			{
				struct Othello::CellPos& pos = RandomItem(places);
				othello_->GetBoard()->Put(pos.x, pos.y, color_);
				return;
			}
		}

		// 角付近1マスを除いて置ける場所がないか探しランダムに選ぶ
		{
			std::vector<struct Othello::CellPos> places;
			std::for_each(poses.begin(), poses.end(), [&](struct Othello::CellPos& pos){
				if (!IsPreCorner(pos.x, pos.y))
				{
					places.push_back(pos);
				}
			});
			if (!places.empty())
			{
				struct Othello::CellPos& pos = RandomItem(places);
				othello_->GetBoard()->Put(pos.x, pos.y, color_);
				return;
			}
		}

		// 上二つが見つからなかった場合ランダムに選ぶ
		struct Othello::CellPos& pos = RandomItem(poses);
		othello_->GetBoard()->Put(pos.x, pos.y, color_);
	}
};
