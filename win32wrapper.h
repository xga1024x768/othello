﻿#pragma once

#include <windows.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <cstring>
#include <memory>

namespace Win32Wrapper
{
	class Window
	{
	private:
		static LRESULT WINAPI WndProcWrapper(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
		{
			LRESULT ret;
			Window* this_;
			this_ = (Window*)GetProp(hWnd, Window::PropertyName_);
			if (msg == WM_CREATE && ((CREATESTRUCT*)lp)->lpCreateParams)
			{
				this_ = (Window*)((CREATESTRUCT*)lp)->lpCreateParams;
			}
			if (this_)
			{
				Window* win = new Window(hWnd);
				ret = this_->WndProc(win, msg, wp, lp);
				delete win;
			}
			else
			{
				ret = DefWindowProc(hWnd, msg, wp, lp);
			}
			return ret;
		}
		WNDPROC OrigWndProc_;
	protected:
		virtual LRESULT WINAPI WndProc(Window* win, UINT msg, WPARAM wp, LPARAM lp)
		{
			return CallWindowProc(OrigWndProc_, hWnd_, msg, wp, lp);
		}
		LRESULT WINAPI CallOrigWindowProc(UINT msg, WPARAM wp, LPARAM lp)
		{
			if (OrigWndProc_) return CallWindowProc(OrigWndProc_, hWnd_, msg, wp, lp);
			return 1;
		}

	protected:
		HINSTANCE ApplicationInstance(){return reinterpret_cast<HINSTANCE>(GetModuleHandle(NULL));}
		HWND hWnd_;
	public:
		static const wchar_t* PropertyName_;
		Window()
		{
		}
		Window(HWND hWnd)
		{
			hWnd_ = hWnd;
			OrigWndProc_ = NULL;
		}
		virtual ~Window()
		{
			DisableSubclassing();
		}

		// XXX: 既に同名プロパティがウィンドウに設定されている場合の処理がない
		void EnableSubclassing()
		{
			OrigWndProc_ = (WNDPROC)GetWindowLongPtr(hWnd_, GWLP_WNDPROC);
			SetProp(hWnd_, Window::PropertyName_, this);
			SetWindowLongPtr(hWnd_, GWLP_WNDPROC, (LONG_PTR)WndProcWrapper);
		}
		void DisableSubclassing()
		{
			if (OrigWndProc_)
			{
				RemoveProp(hWnd_, Window::PropertyName_);
				SetWindowLongPtr(hWnd_, GWLP_WNDPROC, (LONG_PTR)OrigWndProc_);
				OrigWndProc_ = NULL;
			}
		}

		HWND hWnd() { return hWnd_; }

		// アクセサ
		void GetClientPos(POINT* pt)
		{
			RECT rc;
			GetWindowRect(hWnd_, &rc);
			pt->x = rc.left; pt->y = rc.top;
			ClientToScreen(hWnd_, pt);
		}
		int  X()          { POINT pt; GetClientPos(&pt); return pt.x; }
		void X(int value) { Move(value, Y()); }
		int  Y()          { POINT pt; GetClientPos(&pt); return pt.y; }
		void Y(int value) { Move(X(), value); }
		void GetWindowSize(int* width, int* height)
		{
			RECT rc;
			GetWindowRect(hWnd_, &rc);
			*width  = rc.right-rc.left;
			*height = rc.bottom-rc.top;
		}
		int  Width()          { int width, height; GetWindowSize(&width, &height); return width; }
		void Width(int value) { Resize(value, Height()); }
		int  Height()          { int width, height; GetWindowSize(&width, &height); return height; }
		void Height(int value) { Resize(Width(), value); }

		void Show(int nCmdShow)
		{
			ShowWindow(hWnd_, nCmdShow);
			Update();
		}
		void Update()
		{
			UpdateWindow(hWnd_);
		}
		void Move(int x, int y)
		{
			SetWindowPos(hWnd_, 0, x, y, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
		}
		void Resize(int width, int height)
		{
			SetWindowPos(hWnd_, 0, 0, 0, width, height, SWP_NOZORDER | SWP_NOMOVE);
		}

		std::shared_ptr<std::wstring> Text()
		{
			std::shared_ptr<wchar_t> text;
			std::shared_ptr<std::wstring> ret_str(new std::wstring());
			DWORD length;
			length = GetWindowTextLength(hWnd_);
			text = std::shared_ptr<wchar_t>(new wchar_t[length+1]);
			GetWindowText(hWnd_, text.get(), length+1);
			*ret_str = text.get();
			return ret_str;
		}
		void Text(const wchar_t* text)
		{
			SetWindowText(hWnd_, text);
		}

		DWORD WindowExStyle()            { return GetWindowLongPtr(hWnd_, GWL_EXSTYLE); }
		void  WindowExStyle(DWORD value) { SetWindowLongPtr(hWnd_, GWL_EXSTYLE, value); }
		DWORD WindowStyle()            { return GetWindowLongPtr(hWnd_, GWL_STYLE); }
		void  WindowStyle(DWORD value) { SetWindowLongPtr(hWnd_, GWL_STYLE, value); }
		DWORD Id()            { return GetWindowLongPtr(hWnd_, GWL_ID); }
		void  Id(DWORD value) { SetWindowLongPtr(hWnd_, GWL_ID, value); }
		HWND  Parent()         { return GetParent(hWnd_); }
		void  Parent(HWND hWnd){ SetParent(hWnd_, hWnd); }
	};

	// TODO: シングルトンにしたほうが良い？
	class WindowClass
	{
		/////////////////////////////////////////////////
		//
		// ユーティリティ関数
		//
	private:
		// アプリケーションインスタンス
		HINSTANCE ApplicationInstance(){return reinterpret_cast<HINSTANCE>(GetModuleHandle(NULL));}


		/////////////////////////////////////////////////
		//
		// ウィンドウプロシージャ
		//
	private:
		static LRESULT WINAPI WndProcWrapper(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
		{
			LRESULT ret;
			WindowClass* this_;
			this_ = (WindowClass*)GetProp(hWnd, WindowClass::PropertyName_);
			if (msg == WM_CREATE && ((CREATESTRUCT*)lp)->lpCreateParams)
			{
				this_ = (WindowClass*)((CREATESTRUCT*)lp)->lpCreateParams;
			}
			if (this_)
			{
				Window* win = new Window(hWnd);
				ret = this_->WndProc(win, msg, wp, lp);
				delete win;
			}
			else
			{
				ret = DefWindowProc(hWnd, msg, wp, lp);
			}
			return ret;
		}
	protected:
		virtual LRESULT WINAPI WndProc(Window* win, UINT msg, WPARAM wp, LPARAM lp)
		{
			return DefWindowProc(win->hWnd(), msg, wp, lp);
		}


		/////////////////////////////////////////////////
		//
		// ウィンドウの情報
		//
	protected:
		static const wchar_t* PropertyName_;
		const wchar_t* ClassName_;
		const DWORD WindowStyle_;

		/////////////////////////////////////////////////
		//
		// 初期化関係
		//
	private:
		ATOM RegisterWindowClass()
		{
			WNDCLASSEX wc;
			wc.cbSize = sizeof(wc);
			wc.style = CS_HREDRAW | CS_VREDRAW;
			wc.lpfnWndProc = WndProcWrapper;
			wc.cbWndExtra = 0;
			wc.cbClsExtra = 0;
			wc.hInstance = ApplicationInstance();
			wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
			wc.hCursor = LoadCursor(NULL, IDC_ARROW);
			wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
			wc.lpszMenuName = NULL;
			wc.lpszClassName = ClassName_;
			wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
			return RegisterClassEx(&wc);
		}

		HWND CreateNewWindow(HWND hParent=NULL)
		{
			return CreateWindowEx(
				0,
				ClassName_,
				TEXT(""),
				WindowStyle_,
				CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
				hParent, NULL, ApplicationInstance(), this
				);
		}


		/////////////////////////////////////////////////
		//
		// コンストラクタ・デストラクタ
		//
	private:
		// 引数なしコンストラクタの無効化
		WindowClass() : ClassName_(TEXT("")), WindowStyle_(0) {}
	protected:
		WindowClass(const wchar_t* ClassName, DWORD WindowStyle)
			: ClassName_(ClassName), WindowStyle_(WindowStyle) {}
	public:
		std::shared_ptr<Window> Create(HWND hParent=NULL)
		{
			HWND hWnd;
			RegisterWindowClass();
			hWnd = CreateNewWindow(hParent);
			// WM_CREATEで設定したほうがいい？
			SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
			SetProp(hWnd, WindowClass::PropertyName_, this);
			return std::shared_ptr<Window>(new Window(hWnd));
		}
		virtual ~WindowClass()
		{
			UnregisterClass(ClassName_, ApplicationInstance());
		}
	};

	class Button : public Window
	{
	public:
		Button(HWND hParent, const wchar_t* text, int x, int y, int width, int height)
		{
			this->hWnd_ = CreateWindowEx(
				0,
				TEXT("BUTTON"),
				text,
				WS_CHILD | WS_VISIBLE,
				x, y, width, height,
				hParent, NULL, this->ApplicationInstance(), this
				);
		}
	};

	class Edit : public Window
	{
	public:
		Edit(HWND hParent, int x, int y, int width, int height)
		{
			this->hWnd_ = CreateWindowEx(
				0,
				TEXT("EDIT"),
				TEXT(""),
				WS_CHILD | WS_VISIBLE | WS_BORDER,
				x, y, width, height,
				hParent, NULL, this->ApplicationInstance(), this
				);
		}
	};
}
